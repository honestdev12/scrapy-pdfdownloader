# -*- coding: utf-8 -*-
import scrapy
import json
import base64
from urlparse import urlparse, parse_qs, urljoin
import re
import os


class MycheckfreecomSpider(scrapy.Spider):
    name = 'mycheckfree'
    allowed_domains = ['mycheckfree.com', 'secure.bge.com']
    start_urls = ['https://mycheckfree.com']
    idx = 0

    custom_settings = {
        'ROBOTSTXT_OBEY': False,
    }

    def __init__(self, *args, **kwargs):
        super(MycheckfreecomSpider, self).__init__(*args, **kwargs)

        with open('scrapy.log', 'r') as f:
            self.logs = [i.strip() for i in f.readlines()]
            f.close()

    def parse(self, response):
        if not os.path.exists(self.settings.get('DOWNLOAD_DIRECTORY')):
            os.makedirs(self.settings.get('DOWNLOAD_DIRECTORY'))

        return scrapy.FormRequest.from_response(
            response,
            formdata={'ss': 'user_name',
                      'pw': 'password'},
            callback=self.after_login,
            dont_filter=True
        )

    def after_login(self, response):
        tbs = response.xpath('//table[@summary="This table lists all e-bills associated with your bill payment service account"]')
        for tb in tbs:
            trs = tb.xpath('.//tr[td[@class="gridvaluedate"]]')
            if not trs:
                continue
            bill_number = tb.xpath('.//span[@class="gridheadermedium"]/text()').extract_first()
            
            for tr in trs:
                bill_date = tr.xpath('./td[@class="gridvaluedate"]/text()').extract_first()
                view_link = tr.xpath('./td[@class="gridvaluelinks"]/a[contains(text(), "View")]/@onclick').re_first(
                    r'popRemoteBig\(\'(.*?)\',')
                file_link= tr.xpath('./td[@class="gridvaluelinks"]/a[contains(text(), "File")]/@href').extract_first()
                if not self.get_filename(bill_number, bill_date) in self.logs:
                    return response.follow(view_link, callback=self.after_click_view, dont_filter=True, meta={
                        'bill_date': bill_date,
                        'file_link': urljoin(response.url, file_link),
                        'bill_number': bill_number
                    })
        ebills_links = response.xpath('//span[@class="gridheadersmall"]//a[@class="gridheaderlinks" and context(text(), "E-bills")]/@href').extract()
        if self.idx < len(ebills_links):
            link = ebills_links[self.idx]
            self.idx += 1
            return response.follow(link, callback=self.after_click_view_bills, dont_filter=True)
        return

    def after_click_view_bills(self, response):
        bill_number = response.xpath('//td[span[@class="gridheaderstrong"]]//text()').extract_first()
        trs = response.xpath('//tr[@class="gridvalue"]')
        for tr in trs:
            bill_date = tr.xpath('./td[position()=2]/text()').extract_first()
            link = tr.xpath('./td/a[contains(text(), "E-bill")]/@onclick').re_first(r'popRemoteBig\(\'(.*?)\',')
            file_name = self.get_filename(bill_number, bill_date)
            if file_name not in self.logs:
                return response.follow(link, callback=self.after_click_view, dont_filter=True, meta={
                    'bill_date': bill_date,
                    'bill_number': bill_number,
                    'back_request': scrapy.FormRequest.from_response(
                        response,
                        callback=self.after_login
                    )
                })

    def get_filename(self, bill_number, bill_date):
        bill_date = re.findall(r'(\d+)', bill_date)
        bill_date = bill_date[2] + bill_date[0].zfill(2) + bill_date[1].zfill(2) if len(bill_date) == 3 else ''
        return '{}_({}).pdf'.format(bill_number, bill_date)

    def after_click_view(self, response):
        meta = response.meta
        try:
            query = urlparse(response.url).query
            encrypted = parse_qs(query)['securepayload'][0]
            url = ("https://secure.bge.com/api/Services/"
                   "ClientSideUtilityService.svc/GetBillImage")
            headers = {'Content-Type': 'application/json; charset=utf-8'}
            return scrapy.Request(url,
                                  method='POST',
                                  body=json.dumps({'encString': encrypted}),
                                  meta=meta,
                                  headers=headers,
                                  callback=self.after_click_done)
        except KeyError as e:
            file_name = self.get_filename(meta['bill_number'], meta['bill_date'])
            self.logger.warning("Bill {} no longer available".format(file_name))
            self.write_logs(file_name)
            back_request = response.meta.get('back_request')
            if back_request:
               return back_request
            else:
                return scrapy.Request(self.start_urls[0], callback=self.parse, dont_filter=True)

    def after_click_done(self, response):
        meta = response.meta.copy()
        base64encoded = response.body_as_unicode()
        raw_pdf = base64.b64decode(base64encoded)
        fname = self.get_filename(meta.get('bill_number'), meta.get('bill_date'))
        file_name = '{}{}'.format(self.settings.get('DOWNLOAD_DIRECTORY'), fname)
        with open(file_name, 'wb') as f:
            f.write(raw_pdf)
            f.close()
        self.write_logs(file_name)

        self.logger.info("Successfully saved bill in '{}'".format(file_name))
        file_link = response.meta.get('file_link')
        back_request = response.meta.get('back_request')
        if file_link:
            return scrapy.Request(
                response.meta.get('file_link'),
                callback=self.after_login,
                dont_filter=True,
                meta=response.meta
            )
        if back_request:
            return back_request

    def write_logs(self, file_name):
        with open('scrapy.log', 'a') as f:
            f.write(file_name + '\n')
            f.close()
        self.logs.append(file_name)
